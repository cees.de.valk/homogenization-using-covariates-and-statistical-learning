library(latex2exp)


source("addsds.R")
source("/Users/ceesdevalk/Documents/KNMI/Projects/trend_knmi/R/climatrend.R")

# 
# filenames <- 
#   c("Kooy/kooy_tn.csv",
#     "Kooy/kooy_tx.csv",
#     "Eelde/eelde_tn.csv",
#     "Eelde/eelde_tx.csv",
#     "Beek/beek_tn_v3.csv",
#     "Beek/beek_tx_v3.csv",
#     "Vlissingen/vlis_tn.csv",
#     "Vlissingen/vlis_tx.csv"
#     )

filenames <- 
  c("Kooy/kooy_tn_v4.csv_07",
    "Kooy/kooy_tx_v4.csv_07",
    "Eelde/eelde_tn_v4.csv_07",
    "Eelde/eelde_tx_v4.csv_07",
    "Beek/beek_tn_v4.csv_07",
    "Beek/beek_tx_v4.csv_07",
    "Vlissingen/vlis_tn_v4.csv_07",
    "Vlissingen/vlis_tx_v4.csv_07"
  )

othernames <- 
  c("Kooy/etmgeg_235.txt",
    "Kooy/etmgeg_235.txt",
    "Eelde/etmgeg_280.txt",
    "Eelde/etmgeg_280.txt",
    "Beek/etmgeg_380.txt",
    "Beek/etmgeg_380.txt",
    "Vlissingen/etmgeg_310.txt",
    "Vlissingen/etmgeg_310.txt"
  )



# model choices after selection (simplest models with MSE < 1.01 of mimimum MSE)

choices <- c(3, 2, 5, 4, 5, 2, 1, 1)


ns <- length(filenames)



# MSE and skill scores

Stats <- array(NA, dim= c(ns, 3))

Stats <- data.frame(choices= choices)
Stats$filename <- filenames
Stats$formula <- NA
Stats$MSE <- Stats$r2 <- Stats$S1 <- Stats$S2 <- NA
mdlsel <- vector("list", ns)

# overview <- data.frame(date= NULL, x= NULL, other= NULL, homogenized= NULL, homogenized0= NULL, homprev= NULL, sd= NULL, k= NULL)
# # other <- data.frame(date= NULL, measured <- NULL, k= NULL)

overview <- data.frame(date= NULL, x= NULL, x_compl= NULL, y= NULL, 
                hom= NULL, hom0= NULL, hom_compl= NULL, 
                homprev= NULL, homprev_compl= NULL,
                sd= NULL, k= NULL);

start_date <- ymd("1907-01-01")
end_date <- ymd("2022-12-31")
n_days <- interval(start_date,end_date)/days(1)
datearr <- start_date + days(0:n_days)
null <- rep(NA, length(datearr))

#for (k in 1:ns) {
for (k in 1:ns) {
  
  filename <- filenames[k]
  load(paste(filename, ".RData", sep= ""))
  date <- ymd(A$DATUM)
  
  if (k%%2== 1) {
    homp <- A$tn.hom
  } else {
    homp <- A$tx.hom
  }
  
  VAR <- var(df$y[iselect])
  
  Stats$formula[k] <- formulas <- sel$rtable$formula[choices[k]]

  fa <- sel$fa
  r2 <- sel$r2
  
  is <- which(sapply(fa, function(x) formulas %in% x)) 
  js <- which(fa[[is]]== formulas)
  
  i0 <- which(sapply(fa, function(x) "y ~ s(x)"  %in% x)) 
  j0 <- which(fa[[i0]]== "y ~ s(x)")
  
  i1 <- which(sapply(fa, function(x) "y ~ s(x, dcos, dsin)"  %in% x)) 
  j1 <- which(fa[[i1]]== "y ~ s(x, dcos, dsin)")
  
  r2s <- r2[[is]][js]
  r20 <- r2[[i0]][j0]
  r21 <- r2[[i1]][j1]
  r2bn0 <- round((r2s-r20)/(1-r20), 2)
  r2bn1 <- round((r2s-r21)/(1-r21), 2)
  
  Stats$r2[k] <- r2s
  Stats$S1[k] <- r2bn0
  Stats$S2[k] <- r2bn1
  Stats$MSE[k] <- round((1-r2s)*VAR, 2)
  Stats$RMSE[k] <- round(sqrt((1-r2s)*VAR), 2)
  
  B <- read.csv(file= othernames[[k]], skip= 51)
  Bdate <- ymd(B$YYYYMMDD)
  idB <- datearr%in%Bdate
  Bid <- Bdate%in%datearr
  if (k%%2== 1) {
    measured <- B$TN*0.1
  } else {
    measured <- B$TX*0.1
  }

  hom_compl <- hom <- x <- y <- hom0 <- homprev <- sd <- null
  hom_compl[idB] <- measured[Bid]
  x_compl <- homprev_compl <- hom_compl 
  
  mdl <- fitmodel(df[iselect, ], as.formula(formulas))
  # coefsj <- data.frame(sel$coefs[[is]][[js]]) # cross-validation estimates
  mdl <- addsds(mdl, sel$yd, sel$sy)          # adds the sd correction factor for dependence
  mdlsel[[k]] <- mdl
  mdl0 <- fitmodel(df[iselect, ], as.formula("y ~ s(x, dcos, dsin)"))
  
  # homogenisation of full time-series
  prediction <- modelpredict(df, mdl, se.fit= TRUE) 
  prediction0 <- modelpredict(df, mdl0) 
  
  idA <- datearr%in%date
  Aid <- date%in%datearr
  hom[idA] <- hom_compl[idA] <- prediction$fittedvalues[Aid]
  hom0[idA] <- prediction0$fittedvalues[Aid]
  x[idA] <- x_compl[idA] <- df$x[Aid]
  y[idA] <- df$y[Aid]
  homprev[idA] <- homprev_compl[idA] <- homp[Aid]
  kk <- rep(k, length(null))
  sd[idA] <- prediction$sdfit[Aid]
  
  M <- data.frame(date= datearr, x= x, x_compl= x_compl, y= y, 
                  hom= hom, hom0= hom0, hom_compl= hom_compl, 
                  homprev= homprev, homprev_compl= homprev_compl,
                  sd= sd, k= kk);
  overview <- rbind(overview, M)
}



# save(Stats, file= "Stats.RData")
save(Stats, file= "Stats_07b.RData")


overview$rhom <- round(overview$hom)

overview$type <- (overview$k%%2)
overview$type= c("TX", "TN")[overview$type+1]
overview$type= as.factor(overview$type)

overview$site <-  ceiling((overview$k)/2)
overview$site= c("den Helder - de Kooy", "Groningen - Eelde", "Maastricht - Beek", "Souburg - Vlissingen")[overview$site]
overview$site= as.factor(overview$site)

overview$year <- year(overview$date)


save(overview, file= "overview_07b.RData")

# other$type= (other$k%%2)
# other$type= c("TX", "TN")[other$type+1]
# other$type= as.factor(other$type)
# 
# other$site <-  ceiling((other$k)/2)
# other$site= c("de Kooy", "Eelde", "Beek", "Vlissingen")[other$site]
# other$site= as.factor(other$site)
# 
# other$year <- year(other$date)



a <- aggregate(overview$sd, by= list(rhom= overview$rhom, type= overview$type, site= overview$site), FUN= median)

gg <- gg <- ggplot(a, aes(x= rhom, y=x, linetype= type, color= site))
gg <- gg + geom_line(linewidth= 1)
gg <- gg + theme_bw() + theme(aspect.ratio= 1)
gg <- gg + labs(x = "homogenized temperature [deg C]", y = "standard deviation [deg C]")
gg <- gg + coord_cartesian(ylim = c(0, 0.6))
print(gg)
ggsave(file= paste("sd_07b.pdf"), width= 7, height= 5)




# TN

over_n <- overview[overview$type== "TN", ]
b <- aggregate(over_n$hom, by= list(year= over_n$year, site= over_n$site), FUN= min, na.rm= F)
id <- match(b$x, over_n$hom)
b$ymin <- b$x-over_n$sd[id]*1.96
b$ymax <- b$x+over_n$sd[id]*1.96
# bprev <- aggregate(over_n$homprev, by= list(year= over_n$year, site= over_n$site), FUN= min, na.rm= F)
gg <- gg <- ggplot(b, aes(x= year, y=x, color= site))
gg <- gg + geom_line(linewidth= 0.5)
gg <- gg + geom_pointrange(ymin= b$ymin, ymax= b$ymax, size= 0.1)
gg <- gg + theme_bw() + theme(aspect.ratio= 0.4)
gg <- gg + labs(y = "annual minimum temp. [deg C]", x = "year")
gg <- gg + coord_cartesian(xlim= c(1907, 1970))
print(gg)
ggsave(file= paste("annmin_07b.pdf"), width= 7, height= 3)

# homogenized with previous method (dashed)
over_n <- overview[overview$type== "TN", ]
b <- aggregate(over_n$hom, by= list(year= over_n$year, site= over_n$site), FUN= min, na.rm= F)
id <- match(b$x, over_n$hom)
b$ymin <- b$x-over_n$sd[id]*1.96
b$ymax <- b$x+over_n$sd[id]*1.96
bprev <- aggregate(over_n$homprev, by= list(year= over_n$year, site= over_n$site), FUN= min, na.rm= F)
bother <- aggregate(over_n$hom_compl, by= list(year= over_n$year, site= over_n$site), FUN= min, na.rm= F)
gg <- gg <- ggplot(b, aes(x= year, y=x, color= site))
gg <- gg + geom_line(linewidth= 0.4)
gg <- gg + geom_ribbon(aes(ymin= ymin, ymax= ymax, fill= site), alpha= 0.3, linetype= "blank")
gg <- gg + geom_line(data= bprev, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dashed")
gg <- gg + geom_line(data= bother, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dotted")
gg <- gg + theme_bw() + theme(aspect.ratio= 0.4)
gg <- gg + labs(y = "annual minimum temp. [deg C]", x = "year")
gg <- gg + coord_cartesian(xlim= c(1907, 1970))
print(gg)
ggsave(file= paste("annminplus_07b.pdf"), width= 7, height= 3)

# homogenized with simplest model temp + season (dashed)
# and the rest of the RS 
over_n <- overview[overview$type== "TN", ]
b <- aggregate(over_n$hom, by= list(year= over_n$year, site= over_n$site), FUN= min, na.rm= F)
id <- match(b$x, over_n$hom)
b$ymin <- b$x-over_n$sd[id]*1.96
b$ymax <- b$x+over_n$sd[id]*1.96
bprev <- aggregate(over_n$hom0, by= list(year= over_n$year, site= over_n$site), FUN= min, na.rm= F)
bother <- aggregate(over_n$hom_compl, by= list(year= over_n$year, site= over_n$site), FUN= min, na.rm= F)
gg <- gg <- ggplot(b, aes(x= year, y=x, color= site))
gg <- gg + geom_line(linewidth= 0.4)
gg <- gg + geom_ribbon(aes(ymin= ymin, ymax= ymax, fill= site), alpha= 0.3, linetype= "blank")
gg <- gg + geom_line(data= bprev, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dashed")
gg <- gg + geom_line(data= bother, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dotted")
gg <- gg + theme_bw() + theme(aspect.ratio= 0.5)
gg <- gg + labs(y = "annual minimum temp. [deg C]", x = "year")
gg <- gg + coord_cartesian(xlim= c(1907, 1970))
print(gg)
ggsave(file= paste("annminplus0_07b.pdf"), width= 7, height= 3)





# homogenized with simplest model temp + season (dashed)
# and the rest of the RS 
over_n <- overview[overview$type== "TN", ]
b <- aggregate(over_n$hom_compl, by= list(year= over_n$year, site= over_n$site), FUN= min, na.rm= F)
bsm <- b
bsm$xub <- bsm$xlb <- b$x
nl <- length(levels(b$site))
for (l in 1:nl) {
  ind <- b$site== levels(b$site)[l]
  temp <- climatrend(b$year[ind], b$x[ind])
  bsm$x[ind] <- temp$trend
  bsm$xlb[ind] <- temp$trendlbound
  bsm$xub[ind] <- temp$trendubound
}
c <- aggregate(over_n$x_compl, by= list(year= over_n$year, site= over_n$site), FUN= min, na.rm= F)
csm <- c
csm$xub <- csm$xlb <- c$x
nl <- length(levels(c$site))
for (l in 1:nl) {
  ind <- c$site== levels(c$site)[l]
  temp <- climatrend(c$year[ind], c$x[ind], 0.9)
  csm$x[ind] <- temp$trend
  csm$xlb[ind] <- temp$trendlbound
  csm$xub[ind] <- temp$trendubound
}
gg <- gg <- ggplot(bsm, aes(x= year, y=x, color= site))
gg <- gg + geom_line(linewidth= 0.4)
gg <- gg + geom_ribbon(aes(ymin= xlb, ymax= xub, fill= site), alpha= 0.2, linetype= "blank")
gg <- gg + geom_line(data= csm, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dashed")
# gg <- gg + geom_line(data= bother, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dotted")
gg <- gg + theme_bw() + theme(aspect.ratio= 0.6)
gg <- gg + labs(y = "annual minimum temp. [deg C]", x = "year")
gg <- gg + coord_cartesian(xlim= c(1907, 2022))
print(gg)
ggsave(file= paste("annmin_smooth.pdf"), width= 7, height= 4)



# TX

over_n <- overview[overview$type== "TX", ]
b <- aggregate(over_n$hom, by= list(year= over_n$year, site= over_n$site), FUN= max, na.rm= F)
id <- match(b$x, over_n$hom)
b$ymin <- b$x-over_n$sd[id]*1.96
b$ymax <- b$x+over_n$sd[id]*1.96
# bprev <- aggregate(over_n$homprev, by= list(year= over_n$year, site= over_n$site), FUN= max, na.rm= F)
gg <- gg <- ggplot(b, aes(x= year, y=x, color= site))
gg <- gg + geom_line(linewidth= 0.5)
gg <- gg + geom_pointrange(ymin= b$ymin, ymax= b$ymax, size= 0.1)
gg <- gg + theme_bw() + theme(aspect.ratio= 0.4)
gg <- gg + labs(y = "annual maximum temp. [deg C]", x = "year")
gg <- gg + coord_cartesian(xlim= c(1907, 1970))
print(gg)
ggsave(file= paste("annmax_07b.pdf"), width= 7, height= 3)

# homogenized with previous method (dashed)
over_n <- overview[overview$type== "TX", ]
b <- aggregate(over_n$hom, by= list(year= over_n$year, site= over_n$site), FUN= max, na.rm= F)
id <- match(b$x, over_n$hom)
b$ymin <- b$x-over_n$sd[id]*1.96
b$ymax <- b$x+over_n$sd[id]*1.96
bprev <- aggregate(over_n$homprev, by= list(year= over_n$year, site= over_n$site), FUN= max, na.rm= F)
bother <- aggregate(over_n$hom_compl, by= list(year= over_n$year, site= over_n$site), FUN= max, na.rm= F)
gg <- gg <- ggplot(b, aes(x= year, y=x, color= site))
gg <- gg + geom_line(linewidth= 0.4)
gg <- gg + geom_ribbon(aes(ymin= ymin, ymax= ymax, fill= site), alpha= 0.3, linetype= "blank")
gg <- gg + geom_line(data= bprev, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dashed")
gg <- gg + geom_line(data= bother, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dotted")
gg <- gg + theme_bw() + theme(aspect.ratio= 0.4)
gg <- gg + labs(y = "annual maximum temp. [deg C]", x = "year")
gg <- gg + coord_cartesian(xlim= c(1907, 1970))
print(gg)
ggsave(file= paste("annmaxplus_07b.pdf"), width= 7, height= 3)

# homogenized with simplest model temp + season (dashed)
# and the rest of the RS 
over_n <- overview[overview$type== "TX", ]
b <- aggregate(over_n$hom, by= list(year= over_n$year, site= over_n$site), FUN= max, na.rm= F)
id <- match(b$x, over_n$hom)
b$ymin <- b$x-over_n$sd[id]*1.96
b$ymax <- b$x+over_n$sd[id]*1.96
bprev <- aggregate(over_n$hom0, by= list(year= over_n$year, site= over_n$site), FUN= max, na.rm= F)
bother <- aggregate(over_n$hom_compl, by= list(year= over_n$year, site= over_n$site), FUN= max, na.rm= F)
gg <- gg <- ggplot(b, aes(x= year, y=x, color= site))
gg <- gg + geom_line(linewidth= 0.4)
gg <- gg + geom_ribbon(aes(ymin= ymin, ymax= ymax, fill= site), alpha= 0.3, linetype= "blank")
gg <- gg + geom_line(data= bprev, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dashed")
gg <- gg + geom_line(data= bother, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dotted")
gg <- gg + theme_bw() + theme(aspect.ratio= 0.5)
gg <- gg + labs(y = "annual maximum temp. [deg C]", x = "year")
gg <- gg + coord_cartesian(xlim= c(1907, 1970))
print(gg)
ggsave(file= paste("annmaxplus0_07b.pdf"), width= 7, height= 3)



# homogenized with simplest model temp + season (dashed)
# and the rest of the RS 
over_n <- overview[overview$type== "TX", ]
b <- aggregate(over_n$hom_compl, by= list(year= over_n$year, site= over_n$site), FUN= max, na.rm= F)
bsm <- b
bsm$xub <- bsm$xlb <- b$x
nl <- length(levels(b$site))
for (l in 1:nl) {
  ind <- b$site== levels(b$site)[l]
  temp <- climatrend(b$year[ind], b$x[ind])
  bsm$x[ind] <- temp$trend
  bsm$xlb[ind] <- temp$trendlbound
  bsm$xub[ind] <- temp$trendubound
}
c <- aggregate(over_n$x_compl, by= list(year= over_n$year, site= over_n$site), FUN= max, na.rm= F)
csm <- c
csm$xub <- csm$xlb <- c$x
nl <- length(levels(c$site))
for (l in 1:nl) {
  ind <- c$site== levels(c$site)[l]
  temp <- climatrend(c$year[ind], c$x[ind], 0.9)
  csm$x[ind] <- temp$trend
  csm$xlb[ind] <- temp$trendlbound
  csm$xub[ind] <- temp$trendubound
}
gg <- gg <- ggplot(bsm, aes(x= year, y=x, color= site))
gg <- gg + geom_line(linewidth= 0.4)
gg <- gg + geom_ribbon(aes(ymin= xlb, ymax= xub, fill= site), alpha= 0.2, linetype= "blank")
gg <- gg + geom_line(data= csm, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dashed")
# gg <- gg + geom_line(data= bother, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dotted")
gg <- gg + theme_bw() + theme(aspect.ratio= 0.6)
gg <- gg + labs(y = "annual maximum temp. [deg C]", x = "year")
gg <- gg + coord_cartesian(xlim= c(1907, 2022))
print(gg)
ggsave(file= paste("annmax_smooth.pdf"), width= 7, height= 4)





r= .8
ylim= c(-2, 2)
bn <- aggregate(overview$x-overview$y, by= list(month= month(overview$date), site= overview$site, type= overview$type), 
                FUN= mean, na.rm= T)
gg <- gg <- ggplot(bn, aes(x= month, y=x, color= type))
gg <- gg + geom_line()
gg <- gg + geom_point()
gg <- gg + facet_wrap(~ site)
# gg <- gg + geom_ribbon(aes(ymin= xlb, ymax= xub, fill= site), alpha= 0.2, linetype= "blank")
# gg <- gg + geom_line(data= csm, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dashed")
# gg <- gg + geom_line(data= bother, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dotted")
gg <- gg + theme_bw() + theme(aspect.ratio= r)
gg <- gg + labs(y = "monthly mean temp. difference [deg C]", x = "month")
gg <- gg + coord_cartesian(ylim= ylim) + scale_x_continuous(breaks = (1:12)) 
print(gg)
ggsave(file= paste("mmean_x.pdf"), width= 6, height= 5)



bn <- aggregate(overview$hom-overview$y, by= list(month= month(overview$date), site= overview$site, type= overview$type), 
                FUN= mean, na.rm= T)
gg <- gg <- ggplot(bn, aes(x= month, y=x, color= type))
gg <- gg + geom_line()
gg <- gg + geom_point()
gg <- gg + facet_wrap(~ site)
# gg <- gg + geom_ribbon(aes(ymin= xlb, ymax= xub, fill= site), alpha= 0.2, linetype= "blank")
# gg <- gg + geom_line(data= csm, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dashed")
# gg <- gg + geom_line(data= bother, aes(x= year, y=x, color= site), linewidth= 0.4, linetype= "dotted")
gg <- gg + theme_bw() + theme(aspect.ratio= r)
gg <- gg + labs(y = "monthly mean temp. difference [deg C]", x = "month")
gg <- gg + coord_cartesian(ylim= ylim) + scale_x_continuous(breaks = (1:12)) 
print(gg)
ggsave(file= paste("mmean_hom.pdf"), width= 6, height= 5)







library(latex2exp)

# plot contours (wind, cloudiness) Eelde TN

k= 3

filename <- filenames[k]
load(paste(filename, ".RData", sep= ""))

formulas <- sel$rtable$formula[choices[k]]
mdls <- fitmodel(df[iselect,], as.formula(formulas))

png(filename= "Eelde_tn_x0_w0b.png", width= 480, height=  480,  type= "cairo")
par(pty= "s")
vis.gam(mdls$gamdl, view=c("zE", "zN"), cond=list(w= 0, x= 0), 
             color="gray", 
             zlim= c(-5,0.5), lwd= 1.75, 
             plot.type="contour", labcex= 1.5, 
             xlab= TeX("$u_E$", italic= T), ylab= TeX("$u_N$", italic= T), main= "")
dev.off()

png(filename= "Eelde_tn_x0_w10b.png", width= 480, height=  480,  type= "cairo")
par(pty= "s")
vis.gam(mdls$gamdl, view=c("zE", "zN"), cond=list(w= 10, x= 0), 
             color="gray", 
             zlim= c(-5,0.5), lwd= 1.75, 
             plot.type="contour", labcex= 1.5, 
        xlab= TeX("$u_E$", italic= T), ylab= TeX("$u_N$", italic= T), main= "")
dev.off()





# k= 4
# 
# filename <- filenames[k]
# load(paste(filename, ".RData", sep= ""))
# 
# formulas <- sel$rtable$formula[choices[k]]
# mdls <- fitmodel(df[iselect,], as.formula(formulas))
# 
# png(filename= "Eelde_tx_x0_w0.png", width= 480, height=  480,  type= "cairo")
# par(pty= "s")
# vis.gam(mdls$gamdl, view=c("zE", "zN"), cond=list(w= 0, x= 25), 
#         color="gray", 
#         zlim= c(-5,1)+25, lwd= 1.75, 
#         plot.type="contour", labcex= 1.5, 
#         xlab= TeX("$u_E$", italic= T), ylab= TeX("$u_N$", italic= T), main= "")
# dev.off()
# 
# png(filename= "Eelde_tx_x0_w10.png", width= 480, height=  480,  type= "cairo")
# par(pty= "s")
# vis.gam(mdls$gamdl, view=c("zE", "zN"), cond=list(w= 10, x= 25), 
#         color="gray", 
#         zlim= c(-5,1)+25, lwd= 1.75, 
#         plot.type="contour", labcex= 1.5, 
#         xlab= TeX("$u_E$", italic= T), ylab= TeX("$u_N$", italic= T), main= "")
# dev.off()
