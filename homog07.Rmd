---
title: "homog07"
output: pdf_document
date: "2022-12-28"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(mgcv)
library(lubridate)
library(transport)
library(ggplot2)
library(plotly)
library(partitions)
library(reshape2)




```



## generic functions for model evaluation

- first a gam (generalized additive model: smooth function of covariates with adaptive smoothness) is fitted

- then the output is rescaled: variance of predictions is matched to variance of the reference data y (required for homogenisation)

- this is theoretically justified: least-squares regression followed by variance rescaling of the output is equivalent to constraint least squares regression with variance matching as constraint

- this extends to the case of a generalized linear model with a roughness penalty: we can reason that just like the observation mismatch term, the variance matching term needs to be complemented by a (negative) roughness penalty. 


```{r genf, warning=FALSE, results='hide', message=FALSE, fig.width= 4, fig.height= 4}

# utilities

# mse
mse <- function(x) {
  mse <- mean(x^2)
}

# regular 365 day numbers with solution for feb 29 
 daynum <- function(d) {
    year(d) <- 1999  # non-leap year
    num <- yday(d)
    id <- is.na(num) # the feb 29's
    num[id] <- 59.5  # neat!
    return(num)
 }

# gam model fitting and rescaling
# data: the variable to be predicted must be named "y"
# e.g. df <- data.frame(y= A$tn.eelde, x= A$tn.gron)

fitmodel <- function(data, formula) {
  gamdl <- gam(formula, data= data, method="REML")
  # summary(gamdl)
  # gam.check(gamdl)

  # model matrix lp: first col is constant, other cols have mean 0
  lp <- predict(gamdl, newdata= data, type= "lpmatrix", se.fit= TRUE)
  coefs <- gamdl$coefficients
  fact <- sd(data$y)/sd(gamdl$fitted.values)
  lc <- length(coefs)
  
  # rescale coefficients to match the variance of the reference data y 
  coefs[2:lc] <- coefs[2:lc]*fact
  fittedvalues <- lp%*%coefs
  
  # diagnostics (coefficient of determination r2; note that r2 is reduced by rescaling)
  r2 <- 1-mse(fittedvalues-data$y)/var(data$y)

  print(paste("r2 on training data:", round(r2, 2)))
  result <- list(gamdl= gamdl, coefs= coefs, lp= lp, fittedvalues= fittedvalues, 
                 r2= r2, lp= lp, fact= fact)
}

#
# prediction with the rescaled model
#

modelpredict <- function(data, mdl, L= NULL, se.fit= FALSE) {
  lp <- predict(mdl$gamdl, newdata= data, type= "lpmatrix")
  if (length(L)> 0) {
    if (dim(L)[2]== dim(lp)[1]) {
      lp <- L%*%lp
    } else {
      error("L and lp do not match.")
    }
  }
  coefs <- mdl$coefs
  fittedvalues <- lp%*%coefs
  r2 <- NA
  if (length(data$y)> 0) {
    r2 <- 1-mse(fittedvalues-data$y)/var(data$y)
  }
  sdfit <- NULL
  
  if (se.fit) {
    fact <- mdl$fact
    sdfactorjack <- mdl$sdfactorjack
    if (length(sdfactorjack)> 0) {
      n <- length(fittedvalues)
      Vp <- mdl$gamdl$Vp
      nc <- length(coefs)
      Vp[2:nc,] <- Vp[2:nc,]*fact    # apply variance scaling to covariance
      Vp[, 2:nc] <- Vp[, 2:nc]*fact
      
      varmdl <- rep(NA, length= n)
      for (i in 1:n) {
        varmdl[i] <- t(lp[i, ])%*%Vp%*%lp[i, ]
      }    
      # correct standard deviation using jackknife-based correction
      sdfit <- sqrt(varmdl)*sdfactorjack 
    } else {
      error("First run addsds.R for correction of the uncertainty.")
    }
  }  
  print(paste("r2 on test data:", round(r2, 2)))
  result <- list(fittedvalues= fittedvalues, r2= r2, sdfit= sdfit)
}

```
 
 
## Function for selection of covariates

- For models of less than iforward covariates (or fixed covariate combinations): exhaustive search

- For models of iforward or more covariates (covariate combinations): forward selection

- mcovs is the maximum number of covariates/fixed covariate combinations in a model (can be smaller than no. of available  covariates)

- iy is the number of years used for testing


```{r select, warning=FALSE, results='hide', message=FALSE, fig.width= 4, fig.height= 4}


selectmodel <- function(df, date, covnames, mcovs= 10, iforward= 4, iy= 1, icomplex= 4) {
  
  ncovs <- length(covnames)  
  
  # for cross-validation model selection based on years (iy out of ny years for testing; the rest for training)
  
  ny <- length(unique(year(date)))
  sy <- combn(ny, ny-iy)
  nb <- dim(sy)[2]
  yd <- year(date)
  yd <- yd-min(yd)+1
  
  # prepare arrays
  
  mcovs <- min(mcovs, ncovs)
  coefs <- covsn <- vector(mode= "list", mcovs)
  r2 <- fa <- vector(mode= "list", mcovs)
  
  best <- r2best <- rep(NA, mcovs)
  temp0 <- vector(mode= "list", length= 0)
  
  # fit and check, only forward selection
  
  for (i in 1:mcovs) {     
    if (i< iforward) {    # for these i, an exhaustive search is performed,so 
      temp0 <- as.list(unique(c(unlist(temp0))))
    } 
    # list of covariates
    covs <- unique(c(unlist(temp0)))
    # no. of combinations of i variables to try
    id <- which(!((1:ncovs)%in%covs))
    ncomb <- length(id) 
    # all partitions of a set of length(temp0)+1 elements 
    # (the number of groups of covariates (each group modelled by a single smooth) 
    # to consider for each combination)
    parts <- listParts(length(temp0)+1) 
    lp <- length(parts)
    # the complexity of a partition is defined as the length of a largest number of 
    # covariates in a single smooth 
    complexity <- rep(NA, lp) 
    for (l in 1:lp) {
      complexity[l] <- max(as.numeric(unlist(lapply(parts[[l]], length))))
    }
    parts <- parts[complexity<= icomplex]    # partitions which are too complex are removed 
    lp <- length(parts)
    
    # create list to contain the covariate groupings for all the models 
    covsn[[i]] <- vector(mode= "list", ncomb*lp) 
    dim(covsn[[i]]) <- c(ncomb, lp)
    # mdls[[i]] <- preds[[i]] <- covsn[[i]] 
    coefs[[i]] <- covsn[[i]] 
    rr2 <- array(NA, dim= c(ncomb, lp, nb))
    ffa <- vector(mode= "list", ncomb*lp)
    dim(ffa) <- c(ncomb, lp)
    
    for (j in 1:ncomb) {    # no. of combinations equals 
      print(c(i, j/ncomb))
      temp  <- c(temp0, as.list(id[j])) # all groups for the present covariate combination
      
      for (l in 1:lp) {
        llp <- length(parts[[l]])
        covsn[[i]][[j, l]] <- vector(mode= "list", llp)
        for (ll in 1:llp) {
          covsn[[i]][[j, l]][[ll]] <- unlist(temp[parts[[l]][[ll]]]) # we need to merge groups sometimes
        }
        coefs[[i]][[j, l]] <- vector(mode= "list", nb)
        formul <- "y ~ "   # construct the formula for the present model represented by j and l
        for (ll in 1:llp) {
          vars <- paste(covnames[covsn[[i]][[j, l]][[ll]]], collapse= ", ")
          term <- paste("s(", vars, ")", sep= "")
          if (ll< llp) {
            formul <- paste(formul, term, " + ", sep= "")
          } else {
            formul <- paste(formul, term, sep= "")
          }
        } # for ll
        ffa[[j, l]] <- formul
        for (k in 1:nb) {   # training and testing
          # itrain <- md%in%sm[1:3, , k] & yd%in%sy[1:3, 1, k]
          itrain <- yd%in%sy[, k]
          itest <- !itrain
          tempmdl <- fitmodel(df[itrain, ], as.formula(formul))            # train
          coefs[[i]][[j, l]][[k]] <- tempmdl$coefs        # for jackknife
          preds <- modelpredict(df[itest, ], tempmdl)    # test
          rr2[j, l, k] <- preds$r2
        } # for k
      } # for l   
      
    }
    
    r2[[i]] <- apply(rr2, c(1, 2), mean) # mean r2 for each model  
    fa[[i]] <- ffa
    
    best[i] <- which.max(r2[[i]])          
    temp0 <- covsn[[i]][[best[i]]]
    r2best[i] <- r2[[i]][best[i]]
    
    diffs <- round(c(-Inf, diff(log(1-r2best[1:i]))), 2)
    if (any(diffs>= -0.01)) {     # not worth looking further
      break()
    }
       
    # which model to start from in the next round? 
    # the "best" model may be simpler but almost as good as the one with max r2 determined above
    # but then the more complex one will come along later anyway!
    if (i < mcovs) {
      bestid <- arrayInd(best[i], c(ncomb, lp)) # best[i] as array indices
      cbest <- complexity[bestid[2]]            # complexity of its partition
      r2best1 <- r2[[i]][bestid[1], ]           # restrict covariate combinations to the best
      iok <- 1-r2best1< 1.05*(1-r2best[i])      # from these, all decent ones    
      if (i> 1) {
          iok <- iok & 1-r2best1< 1-r2best[i-1] # from these, all the ones which give improvement    
      }
      iok <- iok & complexity< cbest            # from these, all models simpler than best model  
      if (sum(iok)> 0) {                        # best of these
        r2best2 <- max(r2best1[iok])
        iok <- iok & r2best1>= r2best2
        lbest <- min(which(iok))                # pick one
        idbest2 <- bestid[1]+ncomb*(lbest-1)  
        temp0 <- covsn[[i]][[idbest2]]
      }
    }
    
    # save(covnames, iy, mcovs, iforward, 
    #   sy= sy, fa, r2, best, temp0, file= "temp_selectmodel.RData")
  } # for i
  
  # best model and mean r2
  ib <- sum(diffs<= -0.01)
  # ib <- which.max(r2best)
  jb <- best[ib]
  r2b <- r2[[ib]][jb]
  
  # fits of simplest model and best model to full dataset
  formul0 <- fa[[1]][[1]][[1]]
  mdl0 <- fitmodel(df, as.formula(formul0))
  formulb <- fa[[ib]][[jb]][[1]]
  mdlb <- fitmodel(df, as.formula(formulb))
  
  
  # compute correction factors for standard deviation of gam, representing the
  # estimation error only (can be directly applied to the standard deviations from the gam model
  # (using predict(mdlb$gamdl, df, se.fit= TRUE)$se.fit for any data frame df)
  # 
  
  # sd from gam model (including variance matching) for the analyzed period with overlap 
  sdgam <- predict(mdlb$gamdl, df, se.fit= TRUE)$se.fit*mdlb$fact
  
  # full k-fold cross-validation (not needed, already computed, but just to be sure!)
  formulb <- mdlb$gamdl$formula
  itrain <- yd%in%sy[, 1]
  mdl <- fitmodel(df[itrain, ], as.formula(formulb))
  temp <- modelpredict(df, mdl)$fittedvalues
  crossfits <- array(temp, dim= c(length(temp), nb))
  for (k in 2:nb) {
    itrain <- yd%in%sy[, k]
    mdl <- fitmodel(df[itrain, ], as.formula(formulb))
    crossfits[, k] <- modelpredict(df, mdl)$fittedvalues
  }
  
  #  full-length model predictions from k-fold cross validation model estimates
  # temp <- modelpredict(df, mdls[[ib]][[jb]][[1]])$fittedvalues
  # crossfits <- array(temp, dim= c(length(temp), nb))
  # for (k in 2:nb) {
  #   crossfits[, k] <- modelpredict(df, mdls[[ib]][[jb]][[k]])$fittedvalues
  # }
  # 
  # jackknife estimate of standard deviations
  mcross <- apply(crossfits, 1, mean)
  sdcross <- sqrt(apply(crossfits-mcross, 1, mse)*(nb-1))
  a <- sdgam*sqrt(rchisq(length(sdgam), nb-1)/(nb-1))   # this is what a jacknife would "see"
  sdfactorjack <- max(1, median(sdcross)/median(a))     # robust re-scaling (gam model does not overestimate)
  sdjack <- sdgam*sdfactorjack
  
  mdlb$sdfactorjack <- sdfactorjack
  mdlb$sdfittedvalues <- sdjack
 
  # # lag-1 correlation coefficient
  # rho <- acf(df$y-mdlb$fittedvalue, lag.max= 1, type= "correlation", plot= "FALSE")$acf[2]
  # nr <- length(df$y)
  # # appropriate for mean, but applied here to more complex model
  # rhofactor <- sqrt(1+sum((1-(1:nr)/nr)*rho^(1:nr))) 
  # # factor must also account for variance correction factor mdlb$fact
  # sdfactorrho <- mrhofactor
  # sdgamrho <- sdgam*rhofactor
  # 

  # fa <- StripAttr(fa)    # otherwise all the environments get  
  
  r2 <- r2[!unlist(lapply(r2, is.null))]
  mcovs <- length(r2)
  rtable <- melt(r2)
  sr2 <- sort(rtable$value, index.return= TRUE, decreasing= TRUE)
  rtable <- rtable[sr2$ix, ]
  rtable$formula <- NA
  for (i in 1:dim(rtable)[1]) {
    rtable$formula[i] <- fa[[rtable$L1[i]]][[rtable$Var1[i], rtable$Var2[i]]]
  }
  rtable$MSEratio <- round((1-rtable$value)/(1-rtable$value[1]), 3)
  colnames(rtable) <- c("ind1", "ind2", "CV-r2",  "ncovs", "formula","MSEratio")
  
  
  btable <- data.frame(ncovs= 1:mcovs, r2= NA, formula= NA, MSEratio= NA)
  for (i in 1:mcovs) {
    btable$r2[i] <- r2[[i]][best[i]]
    btable$formula[i] <- fa[[i]][[best[i]]]
  }
  btable$MSEratio <- round((1-btable$r2)/(1-max(btable$r2)), 3)
  colnames(btable) <- c("ncovs", "CV-r2", "formula", "MSEratio")


  
  
  value <- list(covnames= covnames, iy= iy, mcovs= mcovs, iforward= iforward, 
                sy= sy, yd= yd, 
                fa= fa, r2= r2, df= df, date= date, 
                covsn= covsn, coefs= coefs, 
                btable= btable, rtable= rtable, 
                best= best, ib= ib, jb= jb, r2b= r2b, 
                mdlb= mdlb, mdl0= mdl0)
  
}

```

## Output: printing and displaying 

-

```{r output, warning=FALSE, results='hide', message=FALSE, fig.width= 4, fig.height= 4}

show.selection <- function(df, hom, sel, filename) {
  
  fa <- sel$fa
  r2 <- sel$r2
  best <- sel$best
  mcovs <- length(r2)
  r2best <- rep(NA, mcovs)
  for (i in 1:mcovs) {
    r2best[i] <- r2[[i]][best[i]]
  }
  ib <- sel$ib
  jb <- sel$jb
  mdlb <- sel$mdlb
  mdl0 <- sel$mdl0
  i0 <- which(sapply(fa, function(x) "y ~ s(x)"  %in% x)) 
  j0 <- which(fa[[i0]]== "y ~ s(x)")
  i1 <- which(sapply(fa, function(x) "y ~ s(x, dcos, dsin)"  %in% x)) 
  j1 <- which(fa[[i1]]== "y ~ s(x, dcos, dsin)")
  
  mdl1 <- fitmodel(df, as.formula("y ~ s(x, dcos, dsin)"))
  r2b <- r2[[ib]][jb]
  r20 <- r2[[i0]][j0]
  r21 <- r2[[i1]][j1]
  r20hom <- 1-mse(hom-df$y)/var(df$y)
  r2bn0 <- round((r2b-r20)/(1-r20), 2)
  r2bn1 <- round((r2b-r21)/(1-r21), 2)
  
  print(" ")
  print(" ")
  # print("All models tested:")
  # for (i in 1:length(fa)) {
  #   print(fa[[i]][[best[i]]], showEnv= FALSE)
  #   print(paste("cross-validation r2:", signif(r2[[i]][best[i]], 3)))
  #   print(" ")
  # }
  print(" ")
  print("Best model:")
  print(mdlb$gamdl$formul)
  print(" ")
  print("residual r2 of best model and its skill relative to simpler models based only on temperature and temperature/season: ")
  
  stats <- c(round(r2b, 2), r2bn0, r2bn1)
  print(stats)
  
  #vis.gam(mdlb$gamdl, view= c("zE", "zN"), cond= list(x= mean(df$x), w= mean(df$w)), se= 1)
  
  
  # 
  # 
  # rtable <- melt(sel$r2)
  # sr2 <- sort(rtable$value, index.return= TRUE, decreasing= TRUE)
  # rtable <- rtable[sr2$ix, ]
  # rtable$formula <- NA
  # for (i in 1:dim(rtable)[1]) {
  #   rtable$formula[i] <- sel$fa[[rtable$L1[i]]][[rtable$Var1[i], rtable$Var2[i]]]
  # }
  # rtable$MSEratio <- round((1-rtable$value)/(1-rtable$value[1]), 3)
  # colnames(rtable) <- c("ind1", "ind2", "CV-r2",  "ncovs", "formula","MSEratio")
  # 
  # 
  # btable <- data.frame(ncovs= 1:sel$mcovs, r2= NA, formula= NA, MSEratio= NA)
  # for (i in 1:sel$mcovs) {
  #   btable$r2[i] <- sel$r2[[i]][sel$best[i]]
  #   btable$formula[i] <- sel$fa[[i]][[sel$best[i]]]
  # }
  # btable$MSEratio <- round((1-btable$r2)/(1-max(btable$r2)), 3)
  # colnames(btable) <- c("ncovs", "CV-r2", "formula", "MSEratio")


  print(" ")
  print(sel$btable)
  print(" ")
  print(sel$rtable)
  print(" ")
 
  
  # mdl5 <- fitmodel(df[itrain, ], y ~ te(x, z, w, dd, k= c(10, 10, 10, 2)))
  # pred5 <- modelpredict(df[itest, ], mdl5)
  
  xlim <- c(floor(min(df$y)), ceiling(max(df$y)))
  ylim <- xlim
  
  par(pty= "s")
  plot(hom, df$y, 
       pch=".", cex=2.5, xlim= xlim, ylim= ylim,
       main= filename, 
       xlab= "Previous version of homogenized temp [C]", 
       ylab= "temp [C]"); grid()
  lines(xlim, ylim, lty= 2)
  lines(sort(hom), sort(df$y), col= "magenta3", lwd= 2) 
  
  par(pty= "s")
  plot(mdlb$fittedvalues, df$y, 
       pch=".", cex=2.5, xlim= xlim, ylim= ylim,
       main= filename, 
       xlab= "Homogenized temp [C] (best gam model)", 
       ylab= "temp [C]"); grid()
  lines(xlim, ylim, lty= 2)
  lines(sort(mdlb$fittedvalues), sort(df$y), col= "magenta3", lwd= 2)  
  
  
  # 
  # 
  # par(pty= "s")
  # plot(mdlb$fittedvalues, df$y-mdlb$fittedvalues, 
  #      pch=".", cex=2.5, xlim= xlim, ylim= c(-5, 5), col= "blue3",
  #      main= "Selected model", 
  #      xlab= "homogenized temp. [C]", 
  #      ylab= "temp. difference [C]"); grid()
  # # lines(xlim, ylim, lty= 2)
  # lines(sort(mdlb$fittedvalues), sort(df$y)-sort(mdlb$fittedvalues), col= "magenta2") 
  # legend("topright", c("residual", "quantile difference"), col= c("blue3", "magenta2"), lty= c(NA, 1), lwd= c(NA, 2), pch= c(".", NA), pt.cex= c(2.5, NA))
  # 
  # 
  # par(pty= "s")
  # plot(mdl1$fittedvalues, df$y-mdl1$fittedvalues, 
  #      pch=".", cex=2.5, xlim= xlim, ylim= c(-5, 5), col= "blue3",
  #      main= "Model based on temp. and season only", 
  #      xlab= "homogenized temp. [C]", 
  #      ylab= "temp. difference [C]"); grid()
  # # lines(xlim, ylim, lty= 2)
  # lines(sort(mdl1$fittedvalues), sort(df$y)-sort(mdl1$fittedvalues), col= "magenta2") 
  # legend("topright", c("residual", "quantile difference"), col= c("blue3", "magenta2"), lty= c(NA, 1), lwd= c(NA, 2), pch= c(".", NA), pt.cex= c(2.5, NA))
  # 
  # 
  # 
  
  
  g <- data.frame(best= mdlb$fittedvalues-df$y, previous= hom-df$y)
  boxplot(g, main= filename, ylab= "residuals [C]")
  grid()
  
  # the following plots need to be redone, because they apply to the fitted gam before rescaling!
  plot(mdlb$gamdl)
  
  stats <- stats
}


# df0 <- df # default (if next step is not applied)


```



## den Helder to de Kooy: tn

- 1906-1970, test/training on 1961-1970

- later: homogenize den Helder up until july 1972

# Fix settings, read data and make dataframe of possible covariates

- This part should be adapted to new datasets or covariate representations


```{r kooy-tn, warning=FALSE, results='hide', message=FALSE, fig.width= 4, fig.height= 4}


# settings

covnames <- c("x", "zE, zN", "w", "h", "sst", "dcos, dsin")  # columns in df to be tested as covariates
# covnames <- c("x", "zE, zN", "w", "dcos, dsin")  # columns in df to be tested as covariates
filename <- "Kooy/kooy_tn_v4.csv"
yr0 <- 1961
yr1 <- 1970

iy <- 1       # no. of years of test data to use
mcovs <- 4    # max. number of covariates (from covnames) in model


# mlengths <- c(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31) 
start_time <- Sys.time()

# read 
# A <- read.csv("test_eelde_2.csv")
A <- read.csv(filename)
date <- ymd(A$DATUM)
yr <- year(date)
iselect <- yr>= yr0 & yr<= yr1


# make a dataframe
# data: the variable to be predicted must be named "y"
# e.g. df <- data.frame(y= A$tn.eelde, x= A$tn.gron)

dd <- daynum(date)                # day number
dcos <- cos(dd/365*2*pi)*5        # scale for balance; not necessary! (results almost the same without)  
dsin <- sin(dd/365*2*pi)*5
ind <- A$DD== 0                   # wind speed correction if wind direction = 0
A$FF[ind] <- 0
zE <- -A$FF*sin(A$DD*(pi/180))    # wind East comp. 
zN <- -A$FF*cos(A$DD*(pi/180))
hom <- A$tn.hom
#w = bewolkingsgraad

df <- data.frame(y= A$tn.kooy, x= A$tn.helder, zE= zE, zN= zN, w= A$N8, h= A$hum8, sst= A$sst, dcos= dcos, dsin= dsin)


sel <- selectmodel(df[iselect, ], date[iselect], covnames, mcovs= mcovs, iy= iy) 
homnew <- modelpredict(df, sel$mdlb)$fittedvalues

save(sel, homnew, A, df, iselect, file= paste(filename, "_07.RData", sep= ""))
show.selection(df[iselect, ], hom[iselect], sel, filename)     

stop_time <- Sys.time()     
stop_time - start_time

```




## den Helder to de Kooy: tx

- 1906-1970, test/training on 1961-1970

- later: homogenize den Helder up until july 1972

# Fix settings, read data and make dataframe of possible covariates

- This part should be adapted to new datasets or covariate representations


```{r kooy-tx, warning=FALSE, results='hide', message=FALSE, fig.width= 4, fig.height= 4}


# settings

covnames <- c("x", "zE, zN", "w", "h", "sst", "dcos, dsin")  # columns in df to be tested as covariates
# covnames <- c("x", "zE, zN", "w", "dcos, dsin")  # columns in df to be tested as covariates
filename <- "Kooy/kooy_tx_v4.csv"
yr0 <- 1961
yr1 <- 1970

iy <- 1       # no. of years of test data to use
mcovs <- 4    # max. number of covariates (from covnames) in model


# mlengths <- c(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31) 
start_time <- Sys.time()

# read 
# A <- read.csv("test_eelde_2.csv")
A <- read.csv(filename)
date <- ymd(A$DATUM)
yr <- year(date)
iselect <- yr>= yr0 & yr<= yr1


# make a dataframe
# data: the variable to be predicted must be named "y"
# e.g. df <- data.frame(y= A$tn.eelde, x= A$tn.gron)

dd <- daynum(date)                # day number
dcos <- cos(dd/365*2*pi)*5        # scale for balance; not necessary! (results almost the same without)  
dsin <- sin(dd/365*2*pi)*5
ind <- A$DD== 0                   # wind speed correction if wind direction = 0
A$FF[ind] <- 0
zE <- -A$FF*sin(A$DD*(pi/180))    # wind East comp. 
zN <- -A$FF*cos(A$DD*(pi/180))
hom <- A$tx.hom
#w = bewolkingsgraad

df <- data.frame(y= A$tx.kooy, x= A$tx.helder, zE= zE, zN= zN, w= A$N14, h= A$hum14, sst= A$sst, dcos= dcos, dsin= dsin)


sel <- selectmodel(df[iselect, ], date[iselect], covnames, mcovs= mcovs, iy= iy) 
homnew <- modelpredict(df, sel$mdlb)$fittedvalues

save(sel, homnew, A, df, iselect, file= paste(filename, "_07.RData", sep= ""))
show.selection(df[iselect, ], hom[iselect], sel, filename)    


stop_time <- Sys.time()     
stop_time - start_time

```





## Groningen to Eelde: tn

- 1907-1951, test/training on 1946-1951

# Fix settings, read data and make dataframe of possible covariates

- This part should be adapted to new datasets or covariate representations


```{r eelde-tn, warning=FALSE, results='hide', message=FALSE, fig.width= 4, fig.height= 4}


# settings

covnames <- c("x", "zE, zN", "w", "h", "dcos, dsin")  # columns in df to be tested as covariates
# covnames <- c("x", "zE, zN", "w", "dcos, dsin")  # columns in df to be tested as covariates
filename <- "Eelde/eelde_tn_v4.csv"
yr0 <- 1946
yr1 <- 1951

iy <- 1       # no. of years of test data to use
mcovs <- 4    # max. number of covariates (from covnames) in model


# mlengths <- c(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31) 
start_time <- Sys.time()

# read 
# A <- read.csv("test_eelde_2.csv")
A <- read.csv(filename)
date <- ymd(A$DATUM)
yr <- year(date)
iselect <- yr>= yr0 & yr<= yr1


# make a dataframe
# data: the variable to be predicted must be named "y"
# e.g. df <- data.frame(y= A$tn.eelde, x= A$tn.gron)

dd <- daynum(date)                # day number
dcos <- cos(dd/365*2*pi)*5        # scale for balance; not necessary! (results almost the same without)  
dsin <- sin(dd/365*2*pi)*5
ind <- A$DD== 0                   # wind speed correction if wind direction = 0
A$FF[ind] <- 0
zE <- -A$FF*sin(A$DD*(pi/180))    # wind East comp. 
zN <- -A$FF*cos(A$DD*(pi/180))
hom <- A$tn.hom
#w = bewolkingsgraad

df <- data.frame(y= A$tn.eelde, x= A$tn.gron, zE= zE, zN= zN, w= A$N8, h= A$hum8, dcos= dcos, dsin= dsin)


sel <- selectmodel(df[iselect, ], date[iselect], covnames, mcovs= mcovs, iy= iy) 
homnew <- modelpredict(df, sel$mdlb)$fittedvalues

save(sel, homnew, A, df, iselect, file= paste(filename, "_07.RData", sep= ""))
show.selection(df[iselect, ], hom[iselect], sel, filename)    

stop_time <- Sys.time()     
stop_time - start_time

```





## Groningen to Eelde: tx

- 1907-1951, test/training on 1946-1951

# Fix settings, read data and make dataframe of possible covariates

- This part should be adapted to new datasets or covariate representations


```{r eelde-tx, warning=FALSE, results='hide', message=FALSE, fig.width= 4, fig.height= 4}


# settings

covnames <- c("x", "zE, zN", "w", "h", "dcos, dsin")  # columns in df to be tested as covariates
# covnames <- c("x", "zE, zN", "w", "dcos, dsin")  # columns in df to be tested as covariates
filename <- "Eelde/eelde_tx_v4.csv"
yr0 <- 1946
yr1 <- 1951

iy <- 1       # no. of years of test data to use
mcovs <- 4    # max. number of covariates (from covnames) in model


# mlengths <- c(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31) 
start_time <- Sys.time()

# read 
# A <- read.csv("test_eelde_2.csv")
A <- read.csv(filename)
date <- ymd(A$DATUM)
yr <- year(date)
iselect <- yr>= yr0 & yr<= yr1


# make a dataframe
# data: the variable to be predicted must be named "y"
# e.g. df <- data.frame(y= A$tn.eelde, x= A$tn.gron)

dd <- daynum(date)                # day number
dcos <- cos(dd/365*2*pi)*5        # scale for balance; not necessary! (results almost the same without)  
dsin <- sin(dd/365*2*pi)*5
ind <- A$DD== 0                   # wind speed correction if wind direction = 0
A$FF[ind] <- 0
zE <- -A$FF*sin(A$DD*(pi/180))    # wind East comp. 
zN <- -A$FF*cos(A$DD*(pi/180))
hom <- A$tx.hom
#w = bewolkingsgraad

df <- data.frame(y= A$tx.eelde, x= A$tx.gron, zE= zE, zN= zN, w= A$N14, h= A$hum14, dcos= dcos, dsin= dsin)


sel <- selectmodel(df[iselect, ], date[iselect], covnames, mcovs= mcovs, iy= iy) 
homnew <- modelpredict(df, sel$mdlb)$fittedvalues

save(sel, homnew, A, df, iselect, file= paste(filename, "_07.RData", sep= ""))
show.selection(df[iselect, ], hom[iselect], sel, filename)    


stop_time <- Sys.time()     
stop_time - start_time

```





## Maastricht to Beek: tn

- 1907-1952, test/training on 1946-1952

# Fix settings, read data and make dataframe of possible covariates

- This part should be adapted to new datasets or covariate representations


```{r beek-tn, warning=FALSE, results='hide', message=FALSE, fig.width= 4, fig.height= 4}


# settings

covnames <- c("x", "zE, zN", "w", "h", "dcos, dsin")  # columns in df to be tested as covariates
# covnames <- c("x", "zE, zN", "w", "dcos, dsin")  # columns in df to be tested as covariates
filename <- "Beek/beek_tn_v4.csv"
yr0 <- 1946
yr1 <- 1952

iy <- 1       # no. of years of test data to use
mcovs <- 4    # max. number of covariates (from covnames) in model


# mlengths <- c(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31) 
start_time <- Sys.time()

# read 
# A <- read.csv("test_eelde_2.csv")
A <- read.csv(filename)
date <- ymd(A$DATUM)
yr <- year(date)
iselect <- yr>= yr0 & yr<= yr1


# make a dataframe
# data: the variable to be predicted must be named "y"
# e.g. df <- data.frame(y= A$tn.eelde, x= A$tn.gron)

dd <- daynum(date)                # day number
dcos <- cos(dd/365*2*pi)*5        # scale for balance; not necessary! (results almost the same without)  
dsin <- sin(dd/365*2*pi)*5
ind <- A$DD== 0                   # wind speed correction if wind direction = 0
A$FF[ind] <- 0
zE <- -A$FF*sin(A$DD*(pi/180))    # wind East comp. 
zN <- -A$FF*cos(A$DD*(pi/180))
hom <- A$tn.hom
#w = bewolkingsgraad

df <- data.frame(y= A$tn.beek, x= A$tn.maas, zE= zE, zN= zN, w= A$N8, h= A$hum8, dcos= dcos, dsin= dsin)


sel <- selectmodel(df[iselect, ], date[iselect], covnames, mcovs= mcovs, iy= iy) 
homnew <- modelpredict(df, sel$mdlb)$fittedvalues

save(sel, homnew, A, df, iselect, file= paste(filename, "_07.RData", sep= ""))
show.selection(df[iselect, ], hom[iselect], sel, filename)    


stop_time <- Sys.time()     
stop_time - start_time

```






## Maastricht to Beek: tx

- 1907-1952, test/training on 1946-1952

# Fix settings, read data and make dataframe of possible covariates

- This part should be adapted to new datasets or covariate representations


```{r beek-tx, warning=FALSE, results='hide', message=FALSE, fig.width= 4, fig.height= 4}


# settings

covnames <- c("x", "zE, zN", "w", "h", "dcos, dsin")  # columns in df to be tested as covariates
# covnames <- c("x", "zE, zN", "w", "dcos, dsin")  # columns in df to be tested as covariates
filename <- "Beek/beek_tx_v4.csv"
yr0 <- 1946
yr1 <- 1952

iy <- 1       # no. of years of test data to use
mcovs <- 4    # max. number of covariates (from covnames) in model


# mlengths <- c(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31) 
start_time <- Sys.time()

# read 
# A <- read.csv("test_eelde_2.csv")
A <- read.csv(filename)
date <- ymd(A$DATUM)
yr <- year(date)
iselect <- yr>= yr0 & yr<= yr1


# make a dataframe
# data: the variable to be predicted must be named "y"
# e.g. df <- data.frame(y= A$tn.eelde, x= A$tn.gron)

dd <- daynum(date)                # day number
dcos <- cos(dd/365*2*pi)*5        # scale for balance; not necessary! (results almost the same without)  
dsin <- sin(dd/365*2*pi)*5
ind <- A$DD== 0                   # wind speed correction if wind direction = 0
A$FF[ind] <- 0
zE <- -A$FF*sin(A$DD*(pi/180))    # wind East comp. 
zN <- -A$FF*cos(A$DD*(pi/180))
hom <- A$tx.hom
#w = bewolkingsgraad

df <- data.frame(y= A$tx.beek, x= A$tx.maas, zE= zE, zN= zN, w= A$N14, h= A$hum14, dcos= dcos, dsin= dsin)


sel <- selectmodel(df[iselect, ], date[iselect], covnames, mcovs= mcovs, iy= iy) 
homnew <- modelpredict(df, sel$mdlb)$fittedvalues

save(sel, homnew, A, df, iselect, file= paste(filename, "_07.RData", sep= ""))
show.selection(df[iselect, ], hom[iselect], sel, filename)    


stop_time <- Sys.time()     
stop_time - start_time

```





## Souburg to Vlissingen: tn

- 1947-1962, test/training on 1959 – 1962 

# Fix settings, read data and make dataframe of possible covariates

- This part should be adapted to new datasets or covariate representations


```{r vlis-tn, warning=FALSE, results='hide', message=FALSE, fig.width= 4, fig.height= 4}


# settings

covnames <- c("x", "zE, zN", "w", "h", "dcos, dsin", "sst")  # columns in df to be tested as covariates
# covnames <- c("x", "zE, zN", "w", "dcos, dsin")  # columns in df to be tested as covariates
filename <- "Vlissingen/vlis_tn_v4.csv"
yr0 <- 1959
yr1 <- 1962
#yr1 <- 1960

iy <- 1       # no. of years of test data to use
mcovs <- 4    # max. number of covariates (from covnames) in model


# mlengths <- c(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31) 
start_time <- Sys.time()

# read 
# A <- read.csv("test_eelde_2.csv")
A <- read.csv(filename)
date <- ymd(A$DATUM)
yr <- year(date)
iselect <- yr>= yr0 & yr<= yr1


# make a dataframe
# data: the variable to be predicted must be named "y"
# e.g. df <- data.frame(y= A$tn.eelde, x= A$tn.gron)

dd <- daynum(date)                # day number
dcos <- cos(dd/365*2*pi)*5        # scale for balance; not necessary! (results almost the same without)  
dsin <- sin(dd/365*2*pi)*5
ind <- A$DD== 0                   # wind speed correction if wind direction = 0
A$FF[ind] <- 0
zE <- -A$FF*sin(A$DD*(pi/180))    # wind East comp. 
zN <- -A$FF*cos(A$DD*(pi/180))
hom <- A$tn.hom
#w = bewolkingsgraad

df <- data.frame(y= A$tn.vlis, x= A$tn.soub, zE= zE, zN= zN, w= A$N8, h= A$hum8, dcos= dcos, dsin= dsin, sst= A$sst)


sel <- selectmodel(df[iselect, ], date[iselect], covnames, mcovs= mcovs, iy= iy) 
homnew <- modelpredict(df, sel$mdlb)$fittedvalues

save(sel, homnew, A, df, iselect, file= paste(filename, "_07.RData", sep= ""))
show.selection(df[iselect, ], hom[iselect], sel, filename)    


stop_time <- Sys.time()     
stop_time - start_time

```





## Souburg to Vlissingen: tx

- 1947-1962, test/training on 1959 – 1962 

# Fix settings, read data and make dataframe of possible covariates

- This part should be adapted to new datasets or covariate representations


```{r vlis-tx, warning=FALSE, results='hide', message=FALSE, fig.width= 4, fig.height= 4}


# settings

covnames <- c("x", "zE, zN", "w", "h", "dcos, dsin", "sst")  # columns in df to be tested as covariates
# covnames <- c("x", "zE, zN", "w", "dcos, dsin")  # columns in df to be tested as covariates
filename <- "Vlissingen/vlis_tx_v4.csv"
yr0 <- 1959
yr1 <- 1962
#yr1 <- 1960

iy <- 1       # no. of years of test data to use
mcovs <- 4    # max. number of covariates (from covnames) in model


# mlengths <- c(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31) 
start_time <- Sys.time()

# read 
# A <- read.csv("test_eelde_2.csv")
A <- read.csv(filename)
date <- ymd(A$DATUM)
yr <- year(date)
iselect <- yr>= yr0 & yr<= yr1


# make a dataframe
# data: the variable to be predicted must be named "y"
# e.g. df <- data.frame(y= A$tn.eelde, x= A$tn.gron)

dd <- daynum(date)                # day number
dcos <- cos(dd/365*2*pi)*5        # scale for balance; not necessary! (results almost the same without)  
dsin <- sin(dd/365*2*pi)*5
ind <- A$DD== 0                   # wind speed correction if wind direction = 0
A$FF[ind] <- 0
zE <- -A$FF*sin(A$DD*(pi/180))    # wind East comp. 
zN <- -A$FF*cos(A$DD*(pi/180))
hom <- A$tx.hom
#w = bewolkingsgraad

df <- data.frame(y= A$tx.vlis, x= A$tx.soub, zE= zE, zN= zN, w= A$N14, h= A$hum14, dcos= dcos, dsin= dsin, sst= A$sst)


sel <- selectmodel(df[iselect, ], date[iselect], covnames, mcovs= mcovs, iy= iy) 
homnew <- modelpredict(df, sel$mdlb)$fittedvalues

save(sel, homnew, A, df, iselect, file= paste(filename, "_07.RData", sep= ""))
show.selection(df[iselect, ], hom[iselect], sel, filename)    


stop_time <- Sys.time()     
stop_time - start_time

```

